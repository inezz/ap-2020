package id.ac.ui.cs.advprog.tutorial2.observer.service;

import id.ac.ui.cs.advprog.tutorial2.observer.core.*;
import id.ac.ui.cs.advprog.tutorial2.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial2.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Agile agileAdventurer;
        private final Knight knightAdventurer;
        private final Mystic mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                this.agileAdventurer = new Agile("Agile", guild);
                this.knightAdventurer = new Knight("Knight", guild);
                this.mysticAdventurer = new Mystic("Mystic", guild);
        }

        @Override
        public void addQuest(Quest quest) {
                questRepository.save(quest);
                this.guild.addQuest(quest);
        }

        @Override
        public List<Adventurer> getAdventurers() {
                return guild.getAdventurers();
        }

        public Agile getAgileAdventurer() {
                return agileAdventurer;
        }

        public Knight getKnightAdventurer() {
                return knightAdventurer;
        }

        public Mystic getMysticAdventurer() {
                return mysticAdventurer;
        }
}
